FROM python:3.11

# clone model
RUN apt-get update && apt-get install -y git git-lfs
RUN git lfs install
RUN git clone https://huggingface.co/facebook/nougat-base /nougat-base
RUN rm -rf /nougat-base.git

RUN pip install --upgrade pip && pip install torch torchvision --index-url https://download.pytorch.org/whl/cpu

WORKDIR /code

ADD requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt
COPY imgs ./imgs
COPY src ./src

CMD python src/main.py
