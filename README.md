# Study Nougat OCR

In this repo I study how to use and prepare a docker container for nougat ocr.

## Hot to run

```
# docker-compose up
docker compose up
# go to localhost:7860
```

## References
* python
* docker, docker compose
* [nougat lib](https://github.com/facebookresearch/nougat)
* [nougat paper info](https://facebookresearch.github.io/nougat/)
* [huggingface explanation of nougat](https://huggingface.co/docs/transformers/model_doc/nougat)
