import gradio as gr

from transformers import NougatProcessor, VisionEncoderDecoderModel
import torch

# from https://huggingface.co/facebook/nougat-base
processor = NougatProcessor.from_pretrained("/nougat-base") #facebook/nougat-base
model = VisionEncoderDecoderModel.from_pretrained("/nougat-base") #facebook/nougat-base
device = "cuda" if torch.cuda.is_available() else "cpu"
model.to(device)


def pred(img):
    pixel_values = processor(img, return_tensors="pt", data_format="channels_first").pixel_values

    outputs = model.generate(
        pixel_values.to(device),
        min_length=1,
        max_new_tokens=30,
        bad_words_ids=[[processor.tokenizer.unk_token_id]],
    )

    sequence = processor.batch_decode(outputs, skip_special_tokens=True)[0]
    sequence = processor.post_process_generation(sequence, fix_markdown=False)
    string_result = repr(sequence)

    return string_result


iface = gr.Interface(
    fn=pred,
    inputs=[gr.inputs.Image(type="pil")],
    outputs=["text"],
    examples=[
        "./imgs/01.jpg",
    ]
)
iface.launch(server_name="0.0.0.0", server_port=7860)
